import os
import shlex

# FIXME Now this configured for windows. Remove '.exe' from files names and '.bat' from 'dmcs'
import shlex

DELAY_BETWEEN_REQUESTS = 1.0
PROJECT_DIR = os.path.dirname(os.path.realpath(__file__))

dirs = {
    'testing_dir': '/testing',
    'tests_dir': '/problems',
    'checkers_dir': '/checkers',
    'gate_archive_dir': '/problems_storage',
}

solution_compilers = {
    'G++': {
        'compile': shlex.split('g++ -std=c++11 -O3 -o solution solution.cxx'),
        'run': ['./solution'],
        'source_filename': 'solution.cxx',
        'solution_filename': 'solution'
    },
    'Python': {
        'compile': False,
        'run': shlex.split('python solution.py'),
        'source_filename': 'solution.py',
        'solution_filename': 'solution.py'
    },
    'C#': {
        'compile': 'dmcs solution.cs'.split(' '),
        'run': ['solution.exe'],
        'source_filename': 'solution.cs',
        'solution_filename': 'solution.exe'
    }
}
default_limits = {
    'compiler': {
        'time_limit': 10000,
        'real_time_limit': 20000,
        'memory_limit': 512000,
        'namespaces': True,
        'seccomp': False,
    },
    'checker': {
        'time_limit': 3000,
        'real_time_limit': 6000,
        'memory_limit': 512000,
        'namespaces': True,
        'seccomp': True,
    },
}



checker_toolchains = {
    'G++': {
        'compile': shlex.split(
            'g++ -I/usr/local/include/ -L/usr/local/lib/ -o check check.cxx -ltestlib -ltestlib++'),
        'run': ['./check'],
        'source_filename': 'check.cxx',
        'checker_filename': 'check',
    },
    'G++_checker': {
        'compile': shlex.split('g++ -O3 -o check check.cxx'),
        'run': ['./check'],
        'source_filename': 'check.cxx',
        'checker_filename': 'check',
    },
    'Python': {
        'compile': False,
        'source_filename': 'wtfchecker.py',
        'checker_filename': 'wtfchecker.py',
        'run': ['python', 'wtfchecker.py']
    },
    'FPC' : {
        'compile': shlex.split('fpc -Fu/usr/local/testlib -Mdelphi -ocheck check.pas'),
        'source_filename': 'check.pas',
        'checker_filename': 'check',
        'run': ['./check'],
    }
}

checker_toolchains['G++1'] = checker_toolchains['G++_checker']
checker_toolchains['DCC'] = checker_toolchains['FPC']

checker = {
    'output_file': 'output.txt',
    'input_file': 'input.txt',
    'answer_file': 'answer.txt',
    'checker_args': shlex.split('input.txt output.txt answer.txt'),
}

saferun_config = {
    'env_helper_path': '/srun2/env_helper',
    'binary_path': '/srun2/srun2'
}

gate = {
    'tester_url': 'http://php-apache/gate/tester/index.php',
    'login': 'login',
    'pass1': 'pass1',
    'pass2': 'pass2',
}
