# Gate и Gate backend

В этом репозитории находится конфигурация докера для запуска dev-окружения тестирующей системы Gate и Gate Backend

![image](/uploads/f197e87ac261c5f0824f4faac26aac20/image.png)

## Установка и запуск
* Склонируйте этот репозиторий, репозитории [Gate](https://github.com/xelez/gate) и [Gate Backend](https://gitlab.com/sicamp-py/gate-backend) в одну папку, как показано ниже:

![image](/uploads/65bfcbd1608f68b183de3f74b6a14952/image.png)

* Установите [Docker](https://www.docker.com/community-edition#/download). Учтите, что необходимо выбрать ту версию docker'а, в которой есть утилита `docker-compose`

* Выполните команду `docker-compose up --build` в папке `gate_docker`

* Подождите, пока docker загрузит все нужные образы и выполнит установку компонентов


После установки попробуйте зайти по одному из адресов, указанных в разделе [Конфигурация](#Конфигурация)

## Пользователи и пароли

### Gate

 * `root` : `assword`
 * `admin` : `admin`

### PhpMyAdmin

 * `php` : `php`

## Конфигурация
`${docker_ip}` - IP текущего контейнера docker (на линукс это обычно `localhost`).
 * Gate: `http://${docker_ip}/gate/` (http://localhost/gate/)
 * PhpMyAdmin: `http://${docker_ip}:8080/` (http://localhost:8080/)
 * БД инициализируется из файла `config/gate_init_db.sql`
 * Конфигурация переписывается файлами из `config/`

# Интеграционные тесты

## Требования и настройка

Для запуска интеграционных тестов нужен Python и пакеты для него, указанные в `requirements.txt`. Установить их можно командой

    pip install -r requirements.txt

Конфигурация интеграционных тестов может быть выполнена в файле [conftest.py](integration_tests/conftest.py). В репозитории он настроен для использования на Ubuntu Linux.

Для начала использования на Windows можно вручную выполнить `docker-compose up --build` и дописать в `conftest.py`:

    @pytest.fixture(scope='session')
    def use_docker():
        return False

    @pytest.fixture(scope='session')
    def gate_address():
        return 'http://192.168.16.1/gate/'

где `192.168.16.1` нужно заменить на IP-адрес docker'а. После этого можно будет запустить тесты и посмотреть на созданные в процессе тестирования задачи и посланные решения.


## Запуск тестов

Запуск выполняется при помощи `pytest`. Удобнее всего запускать тесты из PyCharm, но можно и просто из консоли:

    pytest integration_tests/

Если `use_docker()` возвращает `True`, то автоматически запускается новый набор контейнеров через docker-compose с новыми временными хранилищами данных (volumes) в оперативной памяти. После окончания тестов контейнеры останавливаются. Никаких данных не сохраняется.

Если `use_docker()` возвращает `False`, то тесты используют уже запущенную тестирующую систему, адрес которой возвращается функцией `gate_address()`.