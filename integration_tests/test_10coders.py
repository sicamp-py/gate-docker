import pytest

from .utils.data import problem_path, solution_path

@pytest.fixture(scope='module')
def problem_id(gate):
    return gate.upload_problem(problem_path('10coders'))

@pytest.mark.parametrize('src, expected_result, expected_score', [
    ('ok.cpp', 'Задача зачтена', '100'),
    ('wa.cpp', 'Неверный ответ на тесте #10', '9'),
    ('pe.cpp', 'Неверный формат на тесте #1', '0'),
    ('re.cpp', 'Ошибка исполнения на тесте #2', '1'),
    ('ml.cpp', 'Превышен предел памяти на тесте #8', '7'),
    ('tl.cpp', 'Превышен предел времени на тесте #8', '7'),
])
def test_solution(gate, problem_id, src, expected_result, expected_score):
    src_path = solution_path('10coders/' + src)
    status = gate.submit_solution(problem_id, 'G++', src_path)
    assert status['result'] == expected_result
    assert status['score'] == expected_score

def test_ce(gate, problem_id):
    src_path = solution_path('10coders/ce.cpp')
    status = gate.submit_solution(problem_id, 'G++', src_path)
    assert status['result'] == 'Ошибка компиляции'
    assert 'error: ‘x’ does not name a type' in status['msg']