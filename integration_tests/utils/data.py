import os

DIR = os.path.dirname(os.path.realpath(__file__))
PROBLEMS_PATH = os.path.join(DIR, '../problems')
SOLUTIONS_PATH = os.path.join(DIR, '../solutions')

def problem_path(problem):
    return os.path.join(PROBLEMS_PATH, problem)

def solution_path(solution):
    return os.path.join(SOLUTIONS_PATH, solution)