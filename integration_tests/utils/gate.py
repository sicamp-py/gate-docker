"""Взаимодействие с Gate'ом по средством HTTP запросов


Этот модуль содержит класс для автоматизации тестирования Gate'а
и позволяет создавать контесты, загружать задачи и посылать решения на проверку.
Т.е. позволяет проверить его основную функциональность.

Все исключения, кидаемые данным модулем наследуются от GateError.
TODO: Пока это не правда, ещё бросается TimeoutError из wait_for, нужно обернуть

В модуле используются регулярные выражения, краткое напоминание про используемые в них флаги (?ms):
 * m - re.MULTILINE
 * s - re.DOTALL (точка соответствует всему, даже переводу строки)
"""

import requests
import yaml
import re
import os
import shutil

from .exponential_backoff import wait_for


class GateError(Exception):
    """Базовый класс для всех ошибок, связанных со взаимодействием."""
    pass


class GateApi:
    """Предоставляет функции для взаимодействия с Gate'ом."""

    def __init__(self, base_url='http://localhost/gate/', login='admin', passwd='admin'):
        self.url = base_url
        self.session = requests.Session()
        self.credentials = {'login': login, 'passwd': passwd}
        self._login()

    def _request(self, method, relative_url, *args, **kwargs):
        try:
            return self.session.request(method, self.url + relative_url, *args, **kwargs)
        except requests.exceptions.RequestException as e:
            raise GateError("Can't connect to Gate") from e

    def _get(self, relative_url, *args, **kwargs):
        return self._request('GET', relative_url, *args, **kwargs)

    def _post(self, relative_url, *args, **kwargs):
        return self._request('POST', relative_url, *args, **kwargs)

    def _login(self):
        res = self._post('', data=self.credentials)
        if 'Выйти из системы' not in res.text:
            raise GateError("Can't login: probably wrong login or password")

    def list_checkers(self):
        res = self._get('tester/?page=prbmanager&action=checkers')
        pattern = '\?page=prbmanager&action=checkers&act=edit&id=(\d+)"\>([^<]*)\</a\>\</td\>\<td\>([^<>]*)'
        return re.findall(pattern, res.text)

    def upload_problem(self, name, tests, archive,
                       time_limit=2, memory_limit=256,
                       input_file='input.txt', output_file='output_file.txt',
                       bonus=0, checker_id='', comment='', html_statement=''):
        data = {
            "ProblemSettings_name": name,
            "ProblemSettings_timelimit": time_limit,
            "ProblemSettings_memorylimit": memory_limit,
            "ProblemSettings_input": input_file,
            "ProblemSettings_output": output_file,
            "ProblemSettings_tests": tests,
            "ProblemSettings_bonus": bonus,
            "ProblemSettings_checker": checker_id,
            "ProblemSettings_comment": comment,
            "desc": html_statement,
        }
        files = {
            "TestsArchive": archive,
        }
        self._post('tester/?page=prbmanager&action=create', data=data, files=files)

    def get_problem_id_by_name(self, name):
        res = self._get('tester/?page=prbmanager', params={'filter': name})
        pattern = '(?m)action=view&id=(\d+)'
        ids = re.findall(pattern, res.text)
        if len(ids) != 1:
            raise GateError("Can't get id for problem {!r}".format(name))
        return ids[0]

    def get_problem_status(self, problem_id):
        res = self._get('tester/?page=prbmanager&action=edit', params={'id': problem_id})
        pattern = '(?m)Состояние\</td\>\<td\>([^<]*)'
        state = re.findall(pattern, res.text)
        if len(state) != 1:
            raise GateError("Can't get state for problem {!r}".format(problem_id))
        return state[0]

    def create_contest(self, name: str):
        data = {
            'ContestData_name': name,
            'ContestData_module': 0,
        }
        self._post('tester/?page=contest&action=create', data=data)

    def list_contests(self):
        res = self._get('tester/?page=contest')
        pattern = '(?m)\<a href="\.\?page=contest&changeto=(\d+)"\>([^<]*)</a>'
        return re.findall(pattern, res.text)

    def get_contest_id_by_name(self, name):
        contests = self.list_contests()
        for contest_id, contest_name in contests:
            if name == contest_name:
                return contest_id
        return None

    def add_problem_to_contest(self, problem_id, contest_id):
        self._get('tester/?page=contest&action=manage&cman=1&act=add', params={
            'id': contest_id,
            'uid': problem_id,
        })

    def submit_solution(self, contest_id, problem_id, compiler, src):
        self._get('tester/?page=contest', params={'changeto': contest_id})
        data = {
            'problem_id': problem_id,
            '_compiler': compiler,
            'src': src,
        }
        self._post('tester/?page=submit&action=submit', data=data)

    def get_latest_solution_id(self):
        res = self._get('tester/?page=solutions')
        ids = re.findall('tester/\?page=solutions&action=view&id=(\d+)', res.text)
        if len(ids) == 0:
            raise GateError('No solutions found')
        return ids[0]

    def get_solution_status(self, solution_id):
        res = self._get('tester/?page=solutions&action=view', params={'id': solution_id})

        status = re.search(u'(?m)Статус</td>\s*<td>([^<]*)</td>', res.text).group(1).strip()
        if status == 'Идет тестирование...':
            return {'status': status}

        result = re.search('(?m)<td>Результат</td>\s*<td>([^<]*)</td>', res.text).group(1)
        score = re.search('(?m)Балл</td>\s*<td>([^<]*)</td>', res.text).group(1)
        msg = re.search('(?ms)Сообщения компилятора:<br>\s*<center><textarea[^>]*>(.+?)</textarea>', res.text)
        if msg is not None:
            msg = msg.group(1)

        return {
            'status': status,
            'result': result,
            'score': score,
            'msg': msg
        }

    def add_all_compilers_to_contest(self, contest_id):
        res = self._get('tester/?page=contest&action=manage&cman=3', params={'id': contest_id})
        compilers = re.findall('<input type="checkbox" class="cb" name="([^"]+)"', res.text)
        compilers_dict = {c: 1 for c in compilers}
        self._post('tester/?page=contest&action=manage&cman=3&act=savecompilers',
                   params={'id': contest_id}, data=compilers_dict)


class Gate:
    """Предоставляет высокоуровневые функции для взаимодействия с Gate'ом

    Проводит все действия в рамках одного контеста, название которого
    передаётся в конструкторе.

    Многопоточность и любая параллельность не поддерживается в связи с особенностями Gate'а.
    (Например, чтобы узнать id только что посланного решения нужно посмотреть все решения и взять последнее)
    Также для вытаскивания информации из страниц класс полагается на вёрстку Gate'а и регулярные выражения.
    Увы, сложно придумать что-то лучше в отсутствие API.

    Gate BUG: По какой-то причине нельзя создавать задачи
    пока нет ни одного контеста, так что мы сразу создаём его.
    """

    def __init__(self, gate_api : GateApi, contest_name='TEST_CONTEST'):
        self._api = gate_api
        self.contest_name = contest_name
        self.contest_id = self.create_contest(contest_name)

    def wait_checkers_to_upload(self, timeout=30.0):
        """Ждём, пока скомпилируются стандартные чекеры"""

        def check():
            result = True
            for _, _, status in self._api.list_checkers():
                if not status:
                    continue
                elif status.strip() == 'Закачивание на сервер...':
                    result = False
                else:
                    raise GateError("Error uploading default checkers: {!r}".format(status))
            return result

        wait_for(check, timeout=timeout)

    def create_contest(self, name):
        """Создаёт контест и добавляет на него все компиляторы."""
        self._api.create_contest(name)
        contest_id = self._api.get_contest_id_by_name(name)
        if contest_id is None:
            raise GateError("Couldn't create contest")
        self._api.add_all_compilers_to_contest(contest_id)
        return contest_id

    def upload_problem(self, problem_path, timeout=10.0):
        """Создаёт архив из папки с тестами, создаёт задачу на тестирующей,
        ждёт пока архив загрузится на тестирующую, и добавляет её на контест"""

        problem_config_path = os.path.join(problem_path, 'problem.yml')
        with open(problem_config_path) as conf:
            problem = yaml.safe_load(conf)

        archive_file = shutil.make_archive('temp', 'zip', problem_path)
        with open(archive_file, 'rb') as archive:
            self._api.upload_problem(archive=archive, **problem)

        problem_id = self._api.get_problem_id_by_name(problem['name'])

        def check_uploaded():
            status = self._api.get_problem_status(problem_id).strip()
            if status == 'Закачивание успешно завершено':
                return True
            elif status == 'Закачивание на сервер...':
                return False
            else:
                raise Exception("Error uploading archive: {!r}".format(status))

        wait_for(check_uploaded, timeout=timeout)
        self._api.add_problem_to_contest(problem_id, self.contest_id)
        return problem_id

    def submit_solution(self, problem_id, compiler, src_path, contest_id=None, timeout=60.0):
        """Посылает решения из файла и дожидается его проверки"""
        if contest_id is None:
            contest_id = self.contest_id

        with open(src_path) as src:
            self._api.submit_solution(contest_id, problem_id, compiler, src.read())

        solution_id = self._api.get_latest_solution_id()

        def check_status():
            status = self._api.get_solution_status(solution_id)
            return status['status'] == 'Протестирован'
        wait_for(check_status, timeout=timeout)

        status = self._api.get_solution_status(solution_id)
        status['id'] = solution_id
        return status

