import time


def wait_for(condition, timeout=10.0, max_delay=1.0, min_delay=0.1):
    total_sleep_time = 0.0
    current_delay = min_delay

    while not condition():
        if total_sleep_time > timeout:
            raise TimeoutError("Timeout exceeded while waiting for condition")
        time.sleep(current_delay)
        total_sleep_time += current_delay
        current_delay = min(current_delay * 2, max_delay)
