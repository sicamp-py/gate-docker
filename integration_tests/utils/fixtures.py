import pytest
import requests
import os
import subprocess

from .gate import GateApi, Gate
from .exponential_backoff import wait_for

DIR = os.path.dirname(os.path.realpath(__file__))
DOCKER_COMPOSE_ROOT = os.path.join(DIR, '../../')

# Fixtures for configuration

@pytest.fixture(scope='session')
def docker_compose_cmd():
    return ['gksudo', '--', 'docker-compose', '-f', 'docker-compose.yml', '-f', 'docker-compose.pytest.yml']

@pytest.fixture(scope='session')
def use_docker():
    """Нужно ли стартовать и останавливать контейнеры docker-compose"""
    return True

@pytest.fixture(scope='session')
def gate_address():
    return 'http://localhost/gate/'

# Main fixtures

@pytest.fixture(scope='session')
def docker(use_docker, docker_compose_cmd):
    if not use_docker:
        yield
        return

    cwd = DOCKER_COMPOSE_ROOT
    compose_up_cmd = docker_compose_cmd + ['up', '--build', '-d']
    compose_down_cmd = docker_compose_cmd + ['down']
    logfile_path = os.path.join(cwd, 'docker-compose.pytest.log')

    with open(logfile_path, 'w') as logfile:
        proc = subprocess.run(compose_up_cmd, cwd=cwd, stdout=logfile)
    proc.check_returncode()

    yield

    proc = subprocess.run(compose_down_cmd, cwd=cwd)
    proc.check_returncode()


def is_webserver_up(url):
    try:
        res = requests.get(url)
        return res.status_code == 200
    except requests.exceptions.RequestException:
        return False


@pytest.fixture(scope='session')
def gate(docker, gate_address):
    wait_for(lambda: is_webserver_up(gate_address), timeout=60.0)

    gate_api = GateApi(gate_address, login='admin', passwd='admin')
    gate = Gate(gate_api, 'TEST_CONTEST')
    gate.wait_checkers_to_upload(timeout=30.0)
    return gate
