# Imports all fixtures here
from .utils.fixtures import *

# Fixtures for configuration

'''
# Если нужно, можно раскомментировать и поменять что-нибудь здесь. Полезно для отладки.

@pytest.fixture(scope='session')
def docker_compose_cmd():
    return ['gksudo', '--', 'docker-compose', '-f', 'docker-compose.yml', '-f', 'docker-compose.pytest.yml']

@pytest.fixture(scope='session')
def use_docker():
    """Нужно ли стартовать и останавливать контейнеры docker-compose"""
    return True

@pytest.fixture(scope='session')
def gate_address():
    return 'http://localhost/gate/'
'''
