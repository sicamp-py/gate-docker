#include <iostream>
#include <cstdio>

using namespace std;

int main () {
	freopen("10coders.in", "r", stdin);
	freopen ("10coders.out", "w", stdout);
	int string;
	cin >> string;
	if (string % 2 != 0) {
		cout << 10 - string / 2 << endl;
		return 0;
	}
	if (string % 2 == 0 & string != 20 & string != 22 & string != 18) {
		cout << "1 " << 10 - string / 2 << endl;
		return -1;
	}
	if (string == 18) {
		cout << '1' << endl;
		return 0;
	}
	if (string == 20) {
		cout << '0' << endl;
		return 0;
	}
	if (string == 22) {
		cout << '1' << " 255" << endl;
		return 0;
	}
}
