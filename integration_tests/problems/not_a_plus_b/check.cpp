#include <testlib++.h>

const int MAX = 100;

void Check(){
  int a = inf.ReadLongint();
  int b = inf.ReadLongint();
  int ans = ouf.ReadLongint();
  if (ans > MAX) Quit(_WA, "Too big answer");
  if (ans < -MAX) Quit(_WA, "Too little answer");
  if (ans == a+b) Quit(_WA, "How unlucky of you! ans = a + b!");
 Quit(_OK, "Correct!");
}
